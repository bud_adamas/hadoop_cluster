# =================================================================
# Prepare the Centos images:
docker pull centos:latest

# =================================================================
# Install software in Centos:
#	Java					java-1.8.0-openjdk.x86_64
    yum install -y java-1.8.0-openjdk.x86_64

#	openssh-server			openssh-server.x86_64
    yum install -y openssh-server.x86_64
#   openssh-clients         openssh-clients.x86_64
    yum install -y openssh-clients.x86_64
#   passwd                  passwd.x86_64
    yum install -y passwd.x86_64

#	hadoop  				hadoop-2.5.2

# =================================================================
# configure sshd:
ssh-keygen -t rsa -f /etc/ssh/ssh_host_rsa_key
ssh-keygen -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key
ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key
# do not use PAM, otherwise 'Connection to localhost closed'
sed -i -e 's/UsePAM yes/UsePAM no/' /etc/ssh/sshd_config
# FIXME PubkeyAuthentication yes
# disable SSH host checking
echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config
# start the sshd
/usr/sbin/sshd

# =================================================================
# add a user, with
#   NAME: hadoop
#   HOME: /home/hadoop
#   PASSWORD: hadoop
useradd -d /home/hadoop -m hadoop
passwd hadoop

# change to the *hadoop* user
su hadoop
cd

# =================================================================
# Make a basic new image:
CONTAINER_ID=`docker ps |sed '1d' |cut -d" " -f1`
docker commit ${CONTAINER_ID} adamas/centos

# =================================================================
# get the hadoop software, and untar it.
export HOST_USER=adamas
export HOST_IP=172.16.10.226
export HOST_HOMEDIR=/home/adamas/Downloads/cluster
export HOST_DIR=${HOST_HOMEDIR}/pkg

export HOST_HADOOP_TAR=hadoop-2.6.0.tar.gz
export HOST_SCALA_TAR=scala-2.11.7.tar.gz
export HOST_SPARK_TAR=spark-1.4.1-bin-hadoop2.6.tgz

scp ${HOST_USER}@${HOST_IP}:${HOST_DIR}/${HOST_HADOOP_TAR} .
scp ${HOST_USER}@${HOST_IP}:${HOST_DIR}/${HOST_SCALA_TAR} .
scp ${HOST_USER}@${HOST_IP}:${HOST_DIR}/${HOST_SPARK_TAR} .
tar -zxf ${HOST_HADOOP_TAR}
tar -zxf ${HOST_SCALA_TAR}
tar -zxf ${HOST_SPARK_TAR}

# =================================================================
# set the environment variable in .bash_profile
export JAVA_HOME='/usr/lib/jvm/jre/'
export CLASSPATH=${JAVA_HOME}/lib:.
export HADOOP_HOME=/home/hadoop/hadoop-2.6.0
export SCALA_HOME=/home/hadoop/scala-2.11.7
export SPARK_HOME=/home/hadoop/spark-1.4.1-bin-hadoop2.6
export PATH=$PATH:${JAVA_HOME}/bin:${SCALA_HOME}/bin:${HADOOP_HOME}/bin:${SPARK_HOME}/bin

# =================================================================
# Make a new image, with the software above:
CONTAINER_ID=`docker ps |awk '(NR==2){print}'|cut -d" " -f1`
docker commit ${CONTAINER_ID} adamas/hadoop

# =================================================================
# start the VMs:
docker run -tid --name="namenode" --hostname="namenode" adamas/hadoop /bin/bash

docker run -tid --name="datanode1" --hostname="datanode1" --link=namenode:namenode adamas/hadoop /bin/bash
docker run -tid --name="datanode2" --hostname="datanode2" --link=namenode:namenode adamas/hadoop /bin/bash
docker run -tid --name="datanode3" --hostname="datanode3" --link=namenode:namenode adamas/hadoop /bin/bash
docker run -tid --name="datanode4" --hostname="datanode4" --link=namenode:namenode adamas/hadoop /bin/bash

# =================================================================
# fetch the configuration file automatically
NAMENODE_IP=`docker exec namenode ip addr|\grep "inet.*16"|cut -d"/" -f 1|sed 's/    //g'|cut -d" " -f2`
DATANODE1_IP=`docker exec datanode1 ip addr|\grep "inet.*16"|cut -d"/" -f 1|sed 's/    //g'|cut -d" " -f2`
DATANODE2_IP=`docker exec datanode2 ip addr|\grep "inet.*16"|cut -d"/" -f 1|sed 's/    //g'|cut -d" " -f2`
DATANODE3_IP=`docker exec datanode3 ip addr|\grep "inet.*16"|cut -d"/" -f 1|sed 's/    //g'|cut -d" " -f2`
DATANODE4_IP=`docker exec datanode4 ip addr|\grep "inet.*16"|cut -d"/" -f 1|sed 's/    //g'|cut -d" " -f2`

echo -e "127.0.0.1\tlocalhost" > hadoop_hosts
echo -e "${NAMENODE_IP}\tnamenode"  >> hadoop_hosts
echo -e "${DATANODE1_IP}\tdatanode1" >> hadoop_hosts
echo -e "${DATANODE2_IP}\tdatanode2" >> hadoop_hosts
echo -e "${DATANODE3_IP}\tdatanode3" >> hadoop_hosts
echo -e "${DATANODE4_IP}\tdatanode4" >> hadoop_hosts
echo -e "172.16.10.226\tadamas-gentoo" >> hadoop_hosts

# =================================================================
# copy the file *hadoop_hosts* to other node, including namenode and datanodes,
# to make sure that namenode can ping datanodes listed in file *slaves*

# =================================================================
# generate key for namenode, and then transfer the key to other datanodes,
# to ensure that namenode can login datanodes without keys.
sh-copy-id -i .ssh/id_rsa.pub hadoop@datanode1
sh-copy-id -i .ssh/id_rsa.pub hadoop@datanode2
sh-copy-id -i .ssh/id_rsa.pub hadoop@datanode3
sh-copy-id -i .ssh/id_rsa.pub hadoop@datanode4

# =================================================================
# =================================================================
# configure the hadoop(The following is something important, and copy the files
# from the directory etc/hadoop):
# etc/hadoop/hadoop-env.sh
	export JAVA_HOME='/usr/lib/jvm/jre/'

# conf/core-site.xml
	<property>
        <name>fs.defaultFS</name>
        <value>hdfs://namenode:9000</value>
    </property>

# conf/hdfs-site.xml
      <property>
        <name>hadoop.tmp.dir</name>
        <value>/tmp/hadoop-${user.name}</value>
        <description>A base for other temporary directories.</description>
      </property>

      <property>
	    <name>dfs.namenode.name.dir</name>
	    <value>file://${hadoop.tmp.dir}/dfs/name</value>
	  </property>

	  <property>
	    <name>dfs.datanode.data.dir</name>
	    <value>file://${hadoop.tmp.dir}/dfs/data</value>
	  </property>

	  <property>
	    <name>dfs.replication</name>
	    <value>2</value>
	  </property>

	  <property>
	    <name>dfs.block.size</name>
	    <value>134217728</value>
	  </property>

# =================================================================
# setup the hadoop cluster:
cd /home/hadoop/hadoop-2.6.0

	###### HDFS ######################################################

	# format the namenode
	bin/hdfs namenode -format adamas-cluster

	# start the hdfs
	sbin/start-dfs.sh

    # check the hdfs status(property dfs.namenode.http-address in
    # hdfs-default.xml)
    http://namenode:50070/

	###### YARN ######################################################

	# start resourcemanager for yarn
	sbin/start-yarn.sh

    # check the yarn status
    http://172.17.0.2:8088/

# =================================================================
# setup the spark cluster:
cd /home/hadoop/spark-1.4.1-bin-hadoop2.6

    # start the spark cluter
    sbin/start-all.sh

    # web manager
    http://172.17.0.2:8080/
    http://172.17.0.2:8081/

    # webui
    http://172.17.0.2:4040/



# =================================================================
# an example

# read file in
val readmeFile = sc.textFile("hdfs://namenode:9000/README.md")

# line count
readmeFile.count()

# the count of the lines containing "The"
var theCount = readmeFile.filter(line => line.contains("The"))

# word count
val wordCount = readmeFile.flatMap(line=>line.split(" ")).map(word=>(word,1)).reduceByKey(_+_)
wordCount.collect

